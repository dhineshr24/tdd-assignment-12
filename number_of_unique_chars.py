# https://www.geeksforgeeks.org/count-the-number-of-unique-characters-in-a-given-string/
# Program to count the number of
# unique characters in a string
import os.path


class Unique_chars:

    def functionFromGeeks(self, string):
        if type(string) is not str:
            raise Exception("input not a string!!!")
        # Set to store unique characters in the given string
        st = set([])
        for i in range(len(string)):
            st.add(string[i])
        return len(st)


    def readFromFile(self, file_name):
        if not os.path.exists(file_name):
            raise Exception("wrong file name")
        infile = open(file_name, "r")
        line = infile.readline()
        return line
        # return self.functionFromGeeks(line.split()[0])
