import os
import pytest
from number_of_unique_chars import Unique_chars
from unittest.mock import MagicMock


class Test_Assignment:
    @pytest.fixture
    def unique_chars_fixture(self):
        return Unique_chars()

    @classmethod
    def setup_class(cls):
        print("...setup class for no of unique chars...")

    @classmethod
    def teardown_class(cls):
        print("...setup class for no of unique chars...")

    def test_input_file(self):
        assert os.path.exists("res/input.txt") == 1

    @pytest.mark.parametrize("test_input, expected", [("asa", 2)])
    def test_parameterized(self, unique_chars_fixture, test_input, expected):
        assert unique_chars_fixture.functionFromGeeks(test_input) == expected

    def test_negative_tests(self, unique_chars_fixture):
        with pytest.raises(Exception):
            unique_chars_fixture.unique_chars(-101)
            unique_chars_fixture.unique_chars(2.3)

    def test_correct_out(self, unique_chars_fixture, monkeypatch):
        mock_file = MagicMock()
        mock_file.return_value = "test_value"
        mock_file.readline = MagicMock()
        mock_open = MagicMock()
        mock_open.return_value = mock_file
        monkeypatch.setattr("builtins.open", mock_open)
        mock_exists = MagicMock()
        mock_exists.return_value = True
        monkeypatch.setattr("os.path.exists", mock_exists)
        result = unique_chars_fixture.readFromFile("res/input.txt")
        mock_open.assert_called_once_with("res/input.txt", "r")
        assert result == mock_file.return_value
